package cn.weixin.song.model;

import javax.sql.DataSource;

import com.jcbase.conf.JcConfig;
import com.jcbase.core.plugin.generator.Generator;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.c3p0.C3p0Plugin;

/**
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _CodeGenerator {
	
	public static DataSource getDataSource() {
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = JcConfig.createC3p0Plugin();
		c3p0Plugin.start();
		return c3p0Plugin.getDataSource();
	}
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "cn.weixin.song.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = "/Users/yulong/remote/jcbase-wx-song/src/main/java/cn/weixin/song/model/base";
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "cn.weixin.song.model";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";
		
		// 创建生成器
		Generator gernerator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		// 添加不需要生成的表名
		//gernerator.addExcludedTable("adv");
		//添加需要生成代码的表名
		//gernerator.addCludedTable("app","app_menu");
		gernerator.addCludedTable("music","music_activity","music_activity_question","music_activity_play_record","user_model_play","weixin_welcome","activity_user_play","app","app_menu","user","activity","activity_award","activity_share_record","activity_uesr_play","coupon","coupon_sn","shop");
		// 设置是否在 Model 中生成 dao 对象
		gernerator.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		gernerator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		gernerator.setRemovedTableNamePrefixes("t_");
		// 生成
		gernerator.generate();
	}
}





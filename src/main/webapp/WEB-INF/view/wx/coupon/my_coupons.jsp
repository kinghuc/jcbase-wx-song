<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<title>我的优惠券</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link href="${res_url}wxcj/style.css" rel="stylesheet" type="text/css">
<link href="${res_url}wx/res/common/css/Common.css" rel="stylesheet" />
<link href="${res_url}wx/res/common/css/coupon.css" rel="stylesheet" />
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.1.0.js"></script>
<script type="text/javascript" src="${res_url}wxcj/js/command.js"></script>
</head>
<style>
.tab-box {
display: table;
margin: 10px auto;
border: 1px solid #dcdcdc;
border-radius: 3px;
overflow: hidden;
}
.tab-box a {
display: inline-block;
width: 100px;
text-align: center;
color: #cc3333;
line-height: 40px;
background: #fff;
}
.tab-box .active {
color: #fff;
background: #d92521;
}
.hint_content {
width: 110px;
height: 130px;
position: absolute;
top: 50%;
left: 50%;
margin-top: -65px;
margin-left: -55px;
background-image: url(images/hint.png);
background-size: 100% 100%;
}
</style>
<div id="onloading"></div>
<div id="loading" style="display: none;">
	<div class="loadingbox">
		<section class="ffffbox">
			<div class="loadingpice"></div>
		</section>
	</div>
</div>
<body>
	<div id="halldetail">
			<div class="tab-box">
						<input type="hidden" id="flag" value="2">
							<a href="javascript:;" id="a_1" class="active" onclick="changeDiv(1)">待使用(${l_list_size})</a>
							<a href="javascript:;" id="a_2" onclick="changeDiv(2)">已使用(${r_list_size })</a>
			</div>
			<div class="body" id="div_1">
			<ul class="list_coupon_ul">
				<c:forEach items="${l_list}" var="item">
					<li  onclick="showQrCodeImg('${item.coupon_name}','${item.shop_name }','${item.address }','${item.sn_code }')"><a href="javascript:;">
							<p>${item.coupon_name}<c:if test="${item.frequency_limit gt 0 }"><br/><span style="font-size: 14px;">(${item.frequency_limit }天内只能使用一次)</span></c:if></p>
							<p>
								${item.shop_name }
							</p>
							<p>
							<fmt:formatDate value="${item.start_time }" pattern="yyyy-MM-dd"/>
							至
							<fmt:formatDate value="${item.expire_time }" pattern="yyyy-MM-dd"/>
							</p>
							<p></p>
							<p>使用</p>
							</a>
						</li>
				</c:forEach>
			</ul>
		</div>
		
		<div class="body" id="div_2" style="display: none;">
			<ul class="list_coupon_ul">
			<c:forEach items="${r_list}" var="item">
							<li><a href="javascript:;">
							<p>${item.coupon_name}<c:if test="${item.frequency_limit gt 0 }"><br/><span style="font-size: 14px;">(${item.frequency_limit }天内只能使用一次)</span></c:if></p>
							<p>
								${item.shop_name }
							</p>
							<p>
							消费时间：<fmt:formatDate value="${item.used_time }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</p>
							<p></p> 
							<!-- p style="font-size:15px;line-height:15px;top:18px;width:20px;text-align:center;right:20px;">
								
							</p -->
							<p style="top: 21px;">已消费</p>
							</a>
						</li>
					</c:forEach>
			</ul>
		</div>
		</div>
	</div>
	<div class="gameWin" id="gameWinId">
    	<div class="infoBox">
            <div class="h10"></div>
            <p class="f12">输入您真实的手机号才可使用哦</p>
             <div class="h5"></div>
            <p>
            	<input type="text" name="mobileNumber" id="mobileNumberId" value="" placeholder="填写您的手机号" maxlength="11" />
            </p>
            <div class="h10"></div>
            <div class="infoBtn" id="submitUserInfoId">
            	提 交
            </div>
            <div class="h10"></div>
        </div>
    </div>
	<div class="yeslottery" id="yeslotteryId">
    	<div class="infoBox">
            <h1 id="prizeId" style="color: #fb9759;"></h1>
            <div class="h10"></div>
            <p><img width="200px" id="qr_code_img" src="" alt="" /> </p>
            <div class="h10"></div>
            <p class="f12" id="address"></p>
             <div class="h10"></div>
            <div class="infoBtn" id="infoBtnId02">
               关 闭
            </div>
            <div class="h10"></div>
        </div>
    </div>
</body>

</html>
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="${res_url}wx/res/layer/layer/layer.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		var type="1";
		changeDiv(type);
		document.getElementById('infoBtnId02').onclick=function(){
			document.getElementById('yeslotteryId').style.opacity=0;
			document.getElementById('yeslotteryId').style.webkitTransform='scale(0)';
		}
	});
	function showQrCodeImg(coupon_name,shop_name,address,sn_code){
		if("${user.mobilephone}"==null||"${user.mobilephone}"==""){
			document.getElementById('gameWinId').style.display="block";
			document.getElementById('gameWinId').style.opacity=1;
			document.getElementById('gameWinId').style.webkitTransform='scale(1)';
			document.getElementById('submitUserInfoId').onclick=function(){
				submitUserInfo();
			}
			return ;
		}
		document.getElementById('prizeId').innerHTML=coupon_name;
		$("#address").html(shop_name+"<br/>(地址："+address+")");
		var qr_code_url="${wx_base_url}coupon/qrcode/"+sn_code;
		$("#qr_code_img").attr("src",qr_code_url);
		document.getElementById('yeslotteryId').style.opacity=1;
		document.getElementById('yeslotteryId').style.webkitTransform='scale(1)';
	}
	function submitUserInfo(){
		var mobileNumber=document.getElementById('mobileNumberId').value;
		if(!isMobileNumber(mobileNumber)){
			layer.open({
			    content: '请输入正确的手机号码！',
			    time: 2
			});
			return false;
		}
		var postData={openid:"${user.openid}",mobilephone:mobileNumber};
	    $.post("/wx/luck/submitInfo", postData, function(rs){
	      	 if (rs.code == 0) {
	      			layer.open({
	    			    content: '提交成功',
	    			    time: 2
	    			});
	      			window.location.reload();
               } else {
            	   layer.open({
       			    content: rs.msg,
       			    time: 2
       			});
               }
	      }, 'json');
	}
	function changeDiv(val){
		if(val==1){
			$("#div_1").css("display", "block");
			$("#div_2").css("display", "none");
			$("#a_1").attr("class", "active");
			$("#a_2").attr("class", "");
		}else{
			$("#div_1").css("display", "none");
			$("#div_2").css("display", "block");
			$("#a_2").attr("class", "active");
			$("#a_1").attr("class", "");
		}
	}
	function getCoupon(thi, evt, mcardCouponId,shopId){
		 var type=1;
		 var div=$("#div_1").css("display");
		 if(div=='none'){
			 type=2;
		 }
        evt.stopPropagation();
            $.get("shop/getCoupon?mcardCouponId=" + mcardCouponId +"&shopId="+shopId+"&type="+type, {}, function(rs){
           	 if (rs.code == "success") {
               	  window.location.href="member/coupon?type="+rs.result['type'];
                    } else {
                        alert(rs.result);
                    }
           }, 'json');
    }
</script>
<script>


wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: '${wxAppId}', // 必填，公众号的唯一标识
    timestamp: '${appData.timestamp}', // 必填，生成签名的时间戳
    nonceStr: '${appData.noncestr}', // 必填，生成签名的随机串
    signature: '${appData.signature}',// 必填，签名，见附录1
    jsApiList: ['hideAllNonBaseMenuItem'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
});
	
	wx.ready(function(){
		wx.hideAllNonBaseMenuItem();
		
	}); 
</script>
